const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const cors = require('@koa/cors')
const controllers = require('./controller')
const rest = require('./rest')

const app = new Koa()

app.use(cors())
app.use(rest.restify())
app.use(bodyParser())
app.use(controllers())

app.listen(30203)
