const Sequelize = require('sequelize')
const db = require('../config/db').sequelize
const SqlString = require('sqlstring')
const Util = require('../utils/common')

const getAllStory = async () => {
  let query = `SELECT id,bid,name,date FROM ff_story;`
  let story = await db.query(query)
  if (story && story[0]) {
    return story[0]
  }

  return []
}

const searchStory = async (keyword) => {
  let query = `SELECT id,bid,name,date FROM ff_story`
  if (keyword) {
    keyword = SqlString.escape(`%${keyword}%`)
    query += ` WHERE name LIKE ${keyword}`
  }
  let story = await db.query(query)
  if (story && story[0]) {
    return story[0]
  }

  return []
}

const getDailyStory = async (count) => {
  const currentDate = Util.formatTime(new Date(), 'yyyy-mm-dd')
  let query = `SELECT id,bid,name,description,date FROM ff_story WHERE date='${currentDate}' ORDER BY id DESC;`
  let story = await db.query(query)
  if (story && story[0] && story[0].length) {
    story[0][0].description = JSON.parse(story[0][0].description)
    return story[0][0]
  }
  query = `SELECT bid,name,description FROM ff_story WHERE date < '${currentDate}' ORDER BY date DESC;`
  story = await db.query(query)
  if (story && story[0] && story[0].length) {
    story[0][0].description = JSON.parse(story[0][0].description)
    return story[0][0]
  }

  return {}
}

const getStoryByBrandId = async (id) => {
  let query = `SELECT id,bid,name,description,date FROM ff_story WHERE bid=${id};`
  let story = await db.query(query)
  if (story && story[0] && story[0].length) {
    story[0][0].description = JSON.parse(story[0][0].description)
    return story[0][0]
  }

  return {}
}

const saveBrandStory = async (story) => {
  const isUpdate = !!story.id
  let query
  if (isUpdate) {
    query = SqlString.format(`UPDATE ff_story SET bid=?, date=?, name=?, description=? WHERE id=?;`,
      [story.bid, story.date, story.name, JSON.stringify(story.description), story.id])
  } else {
    query = SqlString.format(`INSERT INTO ff_story (bid, date, name, description) VALUES (?, ?, ?, ?)`,
      [story.bid, story.date, story.name, JSON.stringify(story.description)])
  }
  const result = await db.query(query)

  if (result && result[0]) {
    return result[0]
  }
  return 0
}

module.exports = {
  getAllStory,
  searchStory,
  getDailyStory,
  getStoryByBrandId,
  saveBrandStory
}
