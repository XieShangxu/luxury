const Sequelize = require('sequelize')
const db = require('../config/db').sequelize
const productSchema = db.import('../schema/ff_product.js')
const Util = require('../utils/common')
const Op = Sequelize.Op

const getCurrentDate = () => {
  const date = new Date()
  const year = date.getFullYear()
  let month = date.getMonth() + 1
  let day = date.getDate()
  month = month < 10 ? `0${month}` : month
  day = day < 10 ? `0${day}` : day
  return `${year}${month}${day}`
}

const clearTable = (table) => {
  db.query(`truncate table ff_${table};`);
}

const setBrands = async (brands = []) => {
  const values = brands.reduce((a, b) => `${a},${b}`);
  db.query(`INSERT INTO ff_brand (id, name, gender) VALUES ${values};`)
}

const setCategories = async (categories = []) => {
  const values = categories.reduce((a, b) => `${a},${b}`);
  db.query(`INSERT INTO ff_category (id, name, gender, parentId) VALUES ${values};`)
}

const setProducts = async (products = []) => {
  const values = products.reduce((a, b) => `${a},${b}`);
  db.query(`INSERT INTO ff_product (id, description, brandId, brandName, categoryId, categoryName, imgUrl, gender) VALUES ${values};`)
}

module.exports = {
  clearTable,
  setBrands,
  setCategories,
  setProducts
}
