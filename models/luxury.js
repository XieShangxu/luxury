const Sequelize = require('sequelize')
const db = require('../config/db').sequelize
const Util = require('../utils/common')

const getQuestions = async (count) => {
  const query = `
    SELECT id,description,brandId,brandName,imgUrl
    FROM ff_product
    WHERE brandId NOT IN ('130944', '2857276', '3440', '5220385', '570542')
    ORDER BY RAND()
    LIMIT ${count};
  `
  const questions = await db.query(query)

  return questions[0]
}

const getBrands = async () => {
  const query = 'SELECT id,name FROM ff_brand;'
  const brands = await db.query(query)

  return brands[0]
}

const saveFavorite = async (uid, ffid) => {
  const query = `INSERT INTO ff_favorite (ffid, uid) VALUES ('${ffid}', '${uid}');`
  const result = await db.query(query)

  if (result && result[0]) {
    return result[0]
  }
  return 0
}

const getFavorites = async (uid) => {
  const query = `SELECT ffid FROM ff_favorite WHERE uid='${uid}' ORDER BY id DESC;`
  const favorites = await db.query(query)

  return favorites[0]
}

const getFavoritesWithDetail = async (uid) => {
  const query = `
    SELECT p.id, p.description, p.brandId, p.brandName, p.imgUrl
    FROM ff_favorite AS f
    LEFT JOIN ff_product AS p
    ON f.ffid=p.id
    WHERE f.uid='${uid}'
    ORDER BY f.id DESC;`
  const favorites = await db.query(query)

  return favorites[0]
}

const getProductByIds = async (ids = []) => {
  if (ids.length) {
    const idsStr = ids.reduce((a, b) => `${a},${b}`);
    const query = `SELECT id,description,brandId,brandName,imgUrl FROM ff_product WHERE id IN (${idsStr});`
    const products = await db.query(query)
    return products[0]
  } else {
    return []
  }
}

const deleteFavorite = async (uid, ffid) => {
  if (!uid || !ffid) return 0
  const query = `DELETE FROM ff_favorite WHERE uid='${uid}' AND ffid='${ffid}'`
  const result = await db.query(query)
  if (result && result[0]) {
    return result[0]
  }
  return 0
}

const saveFragment = async (uid, pid) => {
  const query = `INSERT INTO ff_fragment (uid, pid, quantity) VALUES ('${uid}', '${pid}', 1) ON DUPLICATE KEY UPDATE quantity=quantity+1;`
  const result = await db.query(query)

  if (result && result[0]) {
    return result[0]
  }
  return 0
}

const getFragments = async (uid) => {
  const query = `SELECT f.pid AS id, f.quantity, p.imgUrl FROM ff_fragment AS f LEFT JOIN ff_product AS p ON f.pid=p.id WHERE f.uid='${uid}' ORDER BY quantity DESC;`
  const fragments = await db.query(query)

  return fragments[0]
}

const getProfileInfo = async (uid) => {
  const resp = {}
  if (!uid) return resp
  let query = `
    SELECT
    count(*) AS total,
    count(CASE WHEN winner='${uid}' THEN 1 END) AS win
    FROM ff_battle
    WHERE fromer='${uid}'
    OR toer='${uid}'
  `
  const result = await db.query(query)
  resp.total = result[0][0].total
  resp.win = result[0][0].win

  query = `
    SELECT
    SUM(quantity) AS total
    FROM ff_fragment
    WHERE uid='${uid}'
  `
  const result2 = await db.query(query)
  resp.fragment = result2[0][0].total

  return resp
}

const getBattleInfo = async (id) => {
  const query = `SELECT battleId, fromer, toer, questions FROM ff_battle WHERE battleId='${id}';`
  const result = await db.query(query)

  return result[0][0] || {}
}

const saveBattle = async (param) => {
  let { battleId, fromer, toer, fromScore, toScore, winner } = param
  let isNew = false

  if (!battleId) {
    battleId = Util.getGUID().substr(0, 13).replace('-', '')
    isNew = true
  }

  let query = ''
  if (isNew) {
    query = `SELECT id FROM ff_product ORDER BY RAND() LIMIT 10;`
    const questionIds = await db.query(query)
    const questions = questionIds[0].map(q => q.id).join(',')
    query = `
      INSERT INTO ff_battle (battleId, fromer, toer, fromScore, toScore, winner, questions)
      VALUES ('${battleId}', '${fromer}', '${toer || ''}', '${fromScore || 0}', '${toScore || 0}', '${winner || ''}', '${questions}')
    `
    const battle = await db.query(query)
    if (battle) {
      return battleId
    }
  } else {
    query = `
      UPDATE ff_battle SET toer='${toer || ''}', fromScore='${fromScore || 0}', toScore='${toScore || 0}', winner='${winner || ''}'
      WHERE battleId='${battleId}'
    `
    const battle = await db.query(query)
    if (battle) {
      return {}
    }
  }
  return null
}

const makeFriend = async (param) => {
  const { from, to } = param
  
}

module.exports = {
  getQuestions,
  getBrands,
  saveFavorite,
  getFavorites,
  getFavoritesWithDetail,
  getProductByIds,
  deleteFavorite,
  saveFragment,
  getFragments,
  getProfileInfo,
  getBattleInfo,
  saveBattle,
  makeFriend
}
