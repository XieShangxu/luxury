const getGUID = () => {
  let d = new Date().getTime()
  if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
    d += performance.now() // use high-precision timer if available
  }
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = (d + Math.random() * 16) % 16 | 0
    d = Math.floor(d / 16)
    return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16)
  })
}



const formatTime = (date, format = 'dd/mm/yy') => {
  let formattedDate = ''
  const year = date.getFullYear()
  let month = date.getMonth() + 1
  let day = date.getDate()
  let hour = date.getHours()
  let minute = date.getMinutes()

  if (month < 10) { month = `0${month}` }
  if (day < 10) { day = `0${day}` }
  if (hour < 10) { hour = `0${hour}` }
  if (minute < 10) { minute = `0${minute}` }
  formattedDate = format.replace(/yyyy/, year)
  formattedDate = formattedDate.replace(/yy/, `${year}`.substring(2))
  formattedDate = formattedDate.replace(/mm/, month)
  formattedDate = formattedDate.replace(/dd/, day)
  formattedDate = formattedDate.replace(/hh/, hour)
  formattedDate = formattedDate.replace(/MM/, minute)

  return formattedDate
}

module.exports = {
  getGUID,
  formatTime
}
