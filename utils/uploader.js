const path = require('path')
const fs = require('fs')
const Busboy = require('busboy')
const qiniu = require('qiniu')
const { rename } = require('./image')

const mkdirsSync = (dirname) => {
  if (fs.existsSync(dirname)) {
    return true
  } else {
    if (mkdirsSync(path.dirname(dirname))) {
      fs.mkdirSync(dirname)
      return true
    }
  }
  return false
}

const uploadFile = (ctx, options) => {
  const _emmiter = new Busboy({ headers: ctx.req.headers })
  const fileType = options.fileType
  const filePath = path.join(options.path, fileType)
  const confirm = mkdirsSync(filePath)
  if (!confirm) {
    return
  }
  return new Promise((resolve, reject) => {
    _emmiter.on('file', function (fieldname, file, filename, encoding, mimetype) {
      const fileName = rename(filename)
      const saveTo = path.join(path.join(filePath, fileName))
      file.pipe(fs.createWriteStream(saveTo))
      file.on('end', function () {
        resolve({
          imgPath: `/${fileType}/${fileName}`,
          imgKey: fileName
        })
      })
    })

    _emmiter.on('finish', function () {
      console.log('finished...')
    })

    _emmiter.on('error', function (err) {
      console.log('err...')
      reject(err)
    })

    ctx.req.pipe(_emmiter)
  })
}

const upToQiniu = (filePath, key) => {
  const accessKey = 'zcxFkAknbrbtwSrobH6QAWRxiOqRwUTg63r93LHJ'
  const secretKey = '3lV-O6_pAUMawZrSCmklPhsMwOtt_q75D-8aHaXJ'
  const mac = new qiniu.auth.digest.Mac(accessKey, secretKey)

  let options = {
    scope: 'farfetch'
  }

  let putPolicy = new qiniu.rs.PutPolicy(options)
  let uploadToken = putPolicy.uploadToken(mac)

  var config = new qiniu.conf.Config();
  config.zone = qiniu.zone.Zone_z0;

  const formUploader = new qiniu.form_up.FormUploader(config)
  const putExtra = new qiniu.form_up.PutExtra()
  // upload file
  return new Promise((resolved, reject) => {
    formUploader.putFile(uploadToken, key, filePath, putExtra, (respErr, respBody, respInfo) => {
      if (respErr) {
        reject(respErr)
      }
      if (respInfo.statusCode == 200) {
        resolved(respBody)
      } else {
        resolved(respBody)
      }
    })
  })
}

module.exports = {
  uploadFile,
  upToQiniu
}
