const fs = require('fs')

const getSuffix = (fileName) => {
  return fileName.split('.').pop()
}

// rename file
const rename = (fileName) => {
  return Math.random().toString(16).substr(2) + '.' + getSuffix(fileName)
}
// remove file
const removeTemImage = (path) => {
  fs.unlink(path, (err) => {
    if (err) {
      throw err
    }
  })
}

module.exports = {
  rename,
  removeTemImage
}
