const axios = require('axios')
const luxuryModel = require('../models/luxury')
const APIError = require('../rest').APIError
const sampleSize = require('lodash/sampleSize')

module.exports = {
  'GET /api/auth/openid': async (ctx, next) => {
    const code = ctx.request.query.code
    if (!code) {
      throw new APIError('PARAMETER_NOT_VALID', 'CODE不合法')
    }
    const resp = await axios({
      method: 'get',
      url: `https://api.weixin.qq.com/sns/jscode2session?appid=wx52a86f03326bcd21&secret=d2f25b20c3396acfd434eb2635e2e955&js_code=${code}&grant_type=authorization_code`
    })

    ctx.rest(resp.data)
    await next()
  },
  'GET /api/luxurycademy/auth/openid': async (ctx, next) => {
    const code = ctx.request.query.code
    if (!code) {
      throw new APIError('PARAMETER_NOT_VALID', 'CODE不合法')
    }
    const resp = await axios({
      method: 'get',
      url: `https://api.weixin.qq.com/sns/jscode2session?appid=wxdc30853a4db6eea9&secret=920458b9cec34596cae0c3662b4e9474&js_code=${code}&grant_type=authorization_code`
    })

    ctx.rest(resp.data)
    await next()
  },
  'GET /api/luxury/products': async (ctx, next) => {
    const count = ctx.request.query.count || 50
    const uid = ctx.request.query.uid
    const questions = await luxuryModel.getQuestions(count)
    const brands = await luxuryModel.getBrands()
    let favorites = []

    if (uid) {
      favorites = await luxuryModel.getFavorites(uid)
      favorites = favorites.map((f) => f.ffid)
    }

    for (const q of questions) {
      let options = sampleSize(brands, 4)
      let index = -1
      for (const brand of options) {
        if (brand.id === q.brandId) {
          index = options.indexOf(brand)
        }
      }
      if (index >= 0) {
        options.splice(index, 1)
      }
      options = sampleSize(options, 3);
      options.splice(Math.round(Math.random() * 3), 0, {
        id: q.brandId,
        name: q.brandName
      })
      q.options = options

      if (favorites.indexOf(q.id) >= 0) {
        q.isFavorited = true
      } else {
        q.isFavorited = false
      }
    }

    ctx.rest(questions)
    await next()
  },

  'GET /api/luxury/product/:id': async (ctx, next) => {
    const productId = ctx.params.id
    if (!productId) {
      throw new APIError('PARAMETER_NOT_VALID', '商品ID不正确')
    }
    const products = await luxuryModel.getProductByIds([productId])
    const brands = await luxuryModel.getBrands()
    for (const p of products) {
      let options = sampleSize(brands, 4)
      let index = -1
      for (const brand of options) {
        if (brand.id === p.brandId) {
          index = options.indexOf(brand)
        }
      }
      if (index >= 0) {
        options.splice(index, 1)
      }
      options = sampleSize(options, 3);
      options.splice(Math.round(Math.random() * 3), 0, {
        id: p.brandId,
        name: p.brandName
      })
      p.options = options
    }

    ctx.rest(products)
    await next()
  },

  'POST /api/luxury/favorite': async (ctx, next) => {
    const uid = ctx.request.body.uid
    const ffid = ctx.request.body.ffid
    if (!uid || !ffid) {
      throw new APIError('PARAMETER_NOT_VALID', '收藏失败')
    }

    const result = await luxuryModel.saveFavorite(uid, ffid)

    if (result) {
      ctx.rest({
        message: 'success'
      })
    } else {
      throw new APIError('SAVE_FAVORITE_FAIL', '收藏失败')
    }
    
    await next()
  },

  'GET /api/luxury/favorites': async (ctx, next) => {
    const uid = ctx.request.query.uid
    const favorites = await luxuryModel.getFavoritesWithDetail(uid)

    ctx.rest(favorites)
    
    await next()
  },

  'DELETE /api/luxury/favorite': async (ctx, next) => {
    const uid = ctx.request.query.uid
    const ffid = ctx.request.query.ffid
    if (!uid || !ffid) {
      throw new APIError('PARAMETER_NOT_VALID', '您的请求有误')
    }

    const result = luxuryModel.deleteFavorite(uid, ffid)
    if (result) {
      ctx.rest({
        message: 'success'
      })
    } else {
      throw new APIError('DELETE_FAVORITE_FAIL', '删除失败，请重新尝试')
    }
    await next()
  },

  'GET /api/luxury/fragments': async (ctx, next) => {
    const uid = ctx.request.query.uid
    const fragments = await luxuryModel.getFragments(uid)

    ctx.rest(fragments)
    
    await next()
  },

  'POST /api/luxury/fragment': async (ctx, next) => {
    const uid = ctx.request.body.uid
    const pid = ctx.request.body.pid
    if (!uid || !pid) {
      throw new APIError('PARAMETER_NOT_VALID', '参数不完整')
    }

    const result = await luxuryModel.saveFragment(uid, pid)

    if (result) {
      ctx.rest({
        message: 'success'
      })
    } else {
      throw new APIError('SAVE_FRAGMENT_FAIL', '收藏碎片失败')
    }
    
    await next()
  },

  'GET /api/luxury/profile': async (ctx, next) => {
    const uid = ctx.request.query.uid
    const profile = await luxuryModel.getProfileInfo(uid)

    ctx.rest(profile)
    
    await next()
  },

  'GET /api/luxury/battle': async (ctx, next) => {
    const battleId = ctx.request.query.id
    if (!battleId) {
      throw new APIError('PARAMETER_NOT_VALID', '参数不完整')
    }
    const battle = await luxuryModel.getBattleInfo(battleId)

    if (battle && battle.questions) {
      const products = await luxuryModel.getProductByIds(battle.questions.split(','))
      const brands = await luxuryModel.getBrands()
      for (const p of products) {
        let options = sampleSize(brands, 4)
        let index = -1
        for (const brand of options) {
          if (brand.id === p.brandId) {
            index = options.indexOf(brand)
          }
        }
        if (index >= 0) {
          options.splice(index, 1)
        }
        options = sampleSize(options, 3);
        options.splice(Math.round(Math.random() * 3), 0, {
          id: p.brandId,
          name: p.brandName
        })
        p.options = options
      }
      battle.questions = products
    }

    ctx.rest(battle)
    
    await next()
  },

  'POST /api/luxury/battle': async (ctx, next) => {
    const body = ctx.request.body
    if (!body.fromer) {
      throw new APIError('PARAMETER_NOT_VALID', '不能没有发起者')
    }

    const result = await luxuryModel.saveBattle(body)

    if (result) {
      ctx.rest(result)
    } else {
      throw new APIError('GENERATE_BATTLE_FAIL', '发起挑战失败')
    }
    
    await next()
  }
}
