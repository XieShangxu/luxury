const axios = require('axios')
const ffcontentModel = require('../models/ffcontent')
const getGUID = require('../utils/common').getGUID
const APIError = require('../rest').APIError

module.exports = {
  'GET /api/ffcontent/allstory': async (ctx, next) => {
    const stories = await ffcontentModel.getAllStory()

    ctx.rest(stories)
    await next()
  },
  'GET /api/ffcontent/searchstory': async (ctx, next) => {
    const keyword = ctx.request.query.q || ''
    const stories = await ffcontentModel.searchStory(keyword)

    ctx.rest(stories)
    await next()
  },
  'GET /api/ffcontent/daily/story': async (ctx, next) => {
    const story = await ffcontentModel.getDailyStory()

    ctx.rest(story)
    await next()
  },
  'GET /api/ffcontent/story/:id': async (ctx, next) => {
    const brandId = ctx.params.id
    if (!brandId) {
      throw new APIError('PARAMETER_NOT_VALID', '品牌ID不正确')
    }
    const story = await ffcontentModel.getStoryByBrandId(brandId)

    ctx.rest(story)
    await next()
  },
  'POST /api/ffcontent/story': async (ctx, next) => {
    const story = ctx.request.body
    if (!story.date || !story.bid || !story.name || !story.description) {
      throw new APIError('PARAMETER_NOT_VALID', '参数不完整')
    }
    const result = await ffcontentModel.saveBrandStory(story)

    if (result) {
      ctx.rest({
        message: '保存成功'
      })
    } else {
      throw new APIError('SAVE_STORY_FAIL', '保存失败')
    }
    await next()
  },

  'POST /api/ffcontent/qrcode': async (ctx, next) => {
    const body = ctx.request.body
    if (!body.scene || body.scene.length > 32) {
      throw new APIError('PARAMETER_NOT_VALID', '参数不完整')
    }

    const result = await axios({
      method: 'post',
      url: 'https://channel-service-panda.farfetch.net/api/v1/mini-program/qr-codes',
      data: body,
      headers: {
        'X-SUMMER-RequestId': getGUID(),
        Authorization: 'Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IkE3MEFCRTNBNEFBQzIxQzY5MjEwNjdEMkQ3MEFEQUY1MjMwMzUwOUUiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJwd3EtT2txc0ljYVNFR2ZTMXdyYTlTTURVSjQifQ.eyJuYmYiOjE1NDUzODc3OTUsImV4cCI6MTU0NTM5NDk5NSwiaXNzIjoiaHR0cDovL2ZhcmZldGNoLmNvbSIsImF1ZCI6WyJodHRwOi8vZmFyZmV0Y2guY29tL3Jlc291cmNlcyIsImFwaSJdLCJjbGllbnRfaWQiOiJmYXJmZXRjaF9XZUNoYXRTdG9yZSIsImNsaWVudF91aWQiOiIxMDAyNCIsImNsaWVudF90ZW5hbnRJZCI6IjEwMDAwIiwic2NvcGUiOlsiYXBpIl19.S9tyNWBwrQUyPqX7kzD6AH9tf-tdLQnmwjoS5VxHQICGAMrPTY8Tbs4zUk0jCSBwCbMlbJKZHuREhprFm5kywFqIPW_4dRDEkHf10o80CNsDp9mRq3IUPx6piBY8qUjdz4KJbmbU1buHfkwpKrIGfJ3FmCA0x1CdP-a7GopqBfHZTLbq_m0bHJB4hAHlmwlJ35v-hYRkC1b9IV6-iCJgoaKpmbv8l2RrCj97vsqL5Hmaytowxbr0SQQp8bFsivuFnzw9C-Gawnp4E8Jx6apSmPuEsh2RSe51jl828POUs-XwiAsBA7b2VgXAE-LMhaIyHMXVwAFy1PPMVHwcW50Mfg'
      }
    })

    if (result) {
      ctx.rest(result, 'image/jpeg')
    } else {
      throw new APIError('GET_QRCODE_FAIL', '获取小程序码失败')
    }
    await next()
  },

  'GET /api/ffcontent/products': async (ctx, next) => {
    const getProductDetail = (id) => {
      return axios({
        method: 'get',
        url: `https://westore-api.farfetch.cn/api/v1/products/${id}`,
        headers: {
          'X-SUMMER-RequestId': getGUID()
        }
      })
    }
    const ids = ctx.request.query.ids.split(',')
    const resp = await axios.all(ids.map(id => {
      return getProductDetail(id)
    }))

    const products = resp.filter(r => r.status === 200).map(r => {
      const detail = r.data
      const { id, categories, gender } = detail
      const images = [detail.images.find(img => img.size === '300')]
      return {
        id, categories, images, gender
      }
    })

    ctx.rest(products)
    await next()
  },

  'POST /api/ffcontent/user/login': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  },
  'GET /api/ffcontent/user/get_info': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  },
  'POST /api/ffcontent/user/logout': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  },
  'GET /api/ffcontent/message/count': async (ctx, next) => {
    ctx.rest(0)
    await next()
  },
  'GET /api/ffcontent/message/init': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  },
  'GET /api/ffcontent/message/content': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  },
  'POST /api/ffcontent/message/has_read': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  },
  'POST /api/ffcontent/message/remove_readed': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  },
  'POST /api/ffcontent/message/restore': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  },
  'GET /api/ffcontent/get_table_data': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  },
  'GET /api/ffcontent/get_drag_list': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  },
  'POST /api/ffcontent/error_url': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  },
  'POST /api/ffcontent/save_error_logger': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  },
  'POST /api/ffcontent/image/upload': async (ctx, next) => {
    ctx.rest({
      message: 'OK'
    })
    await next()
  }
}
