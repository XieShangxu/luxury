const path = require('path')
const axios = require('axios')
const { removeTemImage } = require('../utils/image')
const { uploadFile, upToQiniu } = require('../utils/uploader')
const { QINIU_DOMAIN } = require('../utils/constant')

module.exports = {
  'POST /api/images/upload': async (ctx, next) => {

    const serverPath = path.join(__dirname, '../uploads/')
    
    // upload file to disk
    const result = await uploadFile(ctx, {
      fileType: 'album',
      path: serverPath
    })
    const imgPath = path.join(serverPath, result.imgPath)

    // upload to qiniu
    const qiniu = await upToQiniu(imgPath, result.imgKey)

    // delete img in disk after uploading to qiniu
    removeTemImage(imgPath)

    const imgUrl = `${QINIU_DOMAIN}${qiniu.key}`

    const postResult = await axios.post('https://syteapi.com/v1.1/offers/bb?account_id=6740&sig=%22FYNCI50Bj9OU%2B5%2FLVVpliJqdkeZkhqkmHKeTLOMIZoM%3D%22', [imgUrl])

    const mappings = Object.values(postResult.data)[0]
    
    // for (let obj of mappings) {
    //   const offerResult = await axios.get(`${obj.offers}&feed=farfetch_gb_en_gbp`)
    //   obj.offers = offerResult.data.ads
    // }

    ctx.rest({
      imgUrl,
      data: mappings
    })
    await next()
  },
  'GET /api/images/recommends': async (ctx, next) => {
    const url = ctx.request.query.offers
    const result = await axios.get(`${decodeURIComponent(url)}&feed=farfetch_gb_en_gbp`)
    ctx.rest({
      data: result.data
    })
  }
}
