const axios = require('axios')
// const articleModel = require('../models/admin')
const getGUID = require('../utils/common').getGUID
const APIError = require('../rest').APIError
const genderMap = require('../utils/constant').genderMap
const adminModel = require('../models/admin')

module.exports = {
  'GET /api/admin/brands': async (ctx, next) => {
    const resp = await axios({
      method: 'get',
      url: 'https://westore-api.farfetch.cn/api/v1/menus/brands',
      headers: {
        'X-SUMMER-RequestId': getGUID()
      }
    })

    await adminModel.clearTable('brand')
    
    const brands = resp.data
    const formattedBrands = []
    const brandList = {}
    for (const gender in brands) {
      for (const cap of brands[gender]) {
        const list = cap.children
        for (const brand of list) {
          if (!brandList[brand.id]) {
            brandList[brand.id] = 1
            formattedBrands.push(`('${brand.id}', '${brand.name.replace(/'/ig, '‘')}', ${genderMap[gender]})`)
          }
        }
      }
    }

    await adminModel.setBrands(formattedBrands)

    ctx.rest({
      message: 'success'
    })
    await next()
  },

  'GET /api/admin/categories': async (ctx, next) => {
    const resp = await axios({
      method: 'get',
      url: 'https://westore-api.farfetch.cn/api/v1/menus/categories',
      headers: {
        'X-SUMMER-RequestId': getGUID()
      }
    })

    await adminModel.clearTable('category')
    
    const categories = resp.data

    const formattedCategories = []
    for (const gender in categories) {
      for (const ctg of categories[gender]) {
        if (ctg.type === 'category') {
          formattedCategories.push(`('${ctg.id}', '${ctg.name.replace(/'/ig, '‘')}', ${genderMap[gender]}, '')`)
          for (const child of ctg.children) {
            if (child.type === 'category') {
              formattedCategories.push(`('${child.id}', '${child.name.replace(/'/ig, '‘')}', ${genderMap[gender]}, '${ctg.id}')`)
            }
          }
        }
      }
    }

    await adminModel.setCategories(formattedCategories)

    ctx.rest({
      message: 'success'
    })
    await next()
  },

  'GET /api/admin/products': async (ctx, next) => {
    const startPage = ctx.request.query.start || 1
    const endPage = ctx.request.query.end || 10
    let param = (page = startPage) => ({
      method: 'get',
      url: 'https://westore-api.farfetch.cn/api/v1/categories/135971/products',
      params: {
        page,
        'page-size': 100,
        'image-sizes': '480',
        gender: 'women',
        sort: 'default',
      },
      headers: {
        'X-SUMMER-RequestId': getGUID()
      }
    })
    const resp = await axios(param())

    if (startPage == 1) {
      await adminModel.clearTable('product')
    }
    
    const products = resp.data ? resp.data.productSummaries : []

    const min = Math.min(endPage, resp.data.totalPages)
    if (resp.data.number < min) {
      for (let i = resp.data.number + 1; i <= min; i++) {
        const result = await axios(param(i))
        const items = result.data.productSummaries || []
        products.push(...items)
      }
    }

    const formattedProducts = []
    const productMap = {}
    for (const product of products) {
      if (!productMap[product.id]) {
        productMap[product.id] = 1
        // id, description, brandId, brandName, categoryId, categoryName, imgUrl, gender
        formattedProducts.push(`('${product.id}', '${product.shortDescription.replace(/'/ig, '‘')}', '${product.brand.id}', '${product.brand.name.replace(/'/ig, '‘')}', '', '', '${product.images[0].url}', 1)`)
      }
    }

    await adminModel.setProducts(formattedProducts)

    ctx.rest(formattedProducts)
    await next()
  }
}
