/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ff_favorite', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    ffid: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    uid: {
      type: DataTypes.STRING(100),
      allowNull: false
    }
  }, {
    tableName: 'ff_favorite'
  });
};
