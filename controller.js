const fs = require('fs')
const path = require('path')
const router = require('koa-router')()

const addMapping = (router, mapping) => {
  for (const url in mapping) {
    if (url.startsWith('GET ')) {
      const path = url.substring(4)
      router.get(path, mapping[url])
    } else if (url.startsWith('POST ')) {
      const path = url.substring(5)
      router.post(path, mapping[url])
    } else if (url.startsWith('DELETE ')) {
      const path = url.substring(7)
      router.delete(path, mapping[url])
    } else {
      console.log('invalid url')
    }
  }
}

const addControllers = (router) => {
  const files = fs.readdirSync(path.resolve(__dirname, 'controllers'))
  const jsFiles = files.filter((file) => file.endsWith('.js'))

  for (const f of jsFiles) {
    const mapping = require(path.resolve(__dirname, 'controllers', f))
    addMapping(router, mapping)
  }
}

module.exports = () => {
  addControllers(router)
  return router.routes()
}
